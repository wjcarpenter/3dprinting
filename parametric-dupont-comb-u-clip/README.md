# Parametric U-clip for Dupont cable organizers

This makes a U-shaped clip for keeping your Dupont cables from jumping out of their organizers. 
I like these organizers, but when moving them around (or, heaven forbid, dropping them),
the cables don't want to stay put.

The clip slides over the comb tips and is held in place by friction. 
For ease of use, you can calculate dimensions based on Dupont organizer comb parameters from 
https://www.thingiverse.com/thing:3705808 or https://www.thingiverse.com/thing:3232719. 
But you can also just give absolute dimensions by setting those parameters to zero values.
I add a couple millimeters to the length to make it easier to remove the clip from the comb.

Since this is friction fit and dealing with tiny dimensions, 
you can be flummoxed by tiny variations in the printing of either combs or the clips. 
Even with repeated prints of the same STL/gcode on the same printer, I see variations.
My solution was to print some of these clips with a few different width adjustments.
For any particular comb, I use one that fits well.
Too loose and it doesn't do its job.
Too tight and it's frustratingly difficult to put on and take off.
Just right is just right.

The canonical location for these files is
https://gitlab.com/wjcarpenter/3dprinting/-/tree/main/parametric-dupont-comb-u-clip.
You can also find them at
https://www.thingiverse.com/thing:5494854.
