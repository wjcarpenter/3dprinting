/* [Explanation] */

// This makes a U-shaped clip for keeping your Dupont cables from jumping out of their organizers. The clip slides over the comb tips and is held in place by friction. For ease of use, you can calculate dimensions based on Dupont organizer comb parameters from https://www.thingiverse.com/thing:3705808 or https://www.thingiverse.com/thing:3232719. But you can also just give absolute dimensions by setting those parameters to zero values.
comment = 1; // 

/* [Comb parameters, from https://www.thingiverse.com/thing:3705808 or https://www.thingiverse.com/thing:3232719 ] */

// Thickness of the comb part; set to 0 to ignore and use absolute thickness adjustment as thickness
combThickness = 4.0;
// Width of a tooth
combToothWidth = 1.2;
// Length of the top part of a tooth
combSharpLength = 1.1;
// Length of the bump part of a tooth; set to 0 to ignore and use absolute height length adjustment as height
toothBumpLength = 3.4;
// Thickness of the bump part of a tooth
toothBumpThickness = 0.7;
// Space between teeth
combToothSpacing = 2.2;
// Number of cable slots; set to 0 to ignore and use absolute length adjustment as length
numCables = 10;

/* [Adjustments] */
// Absolute length adjustment (if the calculation doesn't give you what you want; negative is shorter)
absolute_length_adjustment = 3.1;
// The clip normally just covers the bump in the comb plus this amount; you can make it longer (positive) or shorter (negative)
absolute_height_adjustment = 1.1;
// You normally want the clip to be slightly thinner than the bumps in the combs; you can adjust it here (negative is thinner, positive is thicker).
absolute_thickness_adjustment = -0.651;

/* [Clip] */
// How thick are the floors and walls of the clip?
clipMaterialThickness = 1.1;

/* [Hidden] */
clipLength = (numCables <= 0)
  ? absolute_length_adjustment
  : (numCables + 1) * combToothWidth + numCables * combToothSpacing + absolute_length_adjustment;
clipHeight = (toothBumpLength <= 0)
  ? absolute_height_adjustment
  : combSharpLength + toothBumpLength + absolute_height_adjustment;
clipThickness = (combThickness <= 0)
  ? absolute_thickness_adjustment
  : combThickness + toothBumpThickness + absolute_thickness_adjustment;

rotate([90,0,90])
  difference() {
    cube([clipThickness+2*clipMaterialThickness, clipHeight+clipMaterialThickness, clipLength]);
    translate([clipMaterialThickness, clipMaterialThickness, 0]) {
      cube([clipThickness, clipHeight, clipLength]);
    }
  }
