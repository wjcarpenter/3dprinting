# Parametric battery cover clip

This is the kind of lid that clips in place and typically is covering a cavity where there are batteries.

I have an electric clock that has a 9v back-up battery. A long time ago, the clip part of the cover broke off and has been lost. I have the rest of the cover, so it was easy to measure that part with my calipers. I had complete artistic freedeom to imagine what the clip part should look like.

If you try to make this, you will probably have to do some experimental prints until you get it exactly right. For the dimensions, you want to get it snug, but not so snug that it's hard to get it in and out. You want the clip to be stiff enough to hold things in place but flexible enough to make it reasonable to remove it without a jackhammer. But it's a small part, so you won't be wasting too much time or filament until you get it dialed in.

The permanent home of this is at https://gitlab.com/wjcarpenter/3dprinting/-/tree/main/parametric-battery-cover-clip
This design is also published at https://www.thingiverse.com/thing:5429380

Besides the purely dimensional stuff, customization also allows you to choose:

- The stub cross-section shape can be triangular or rectangular.
- You can omit the fillet where the clip attaches to the cover.
- You can omit the cylinder at the "V" in the clip. (I'm not a mechanical engineer, so I'm not really sure if it actually adds strength. It's entirely possible that it reduces strength. It at least *looks* like it adds strength.)

The dimensions and other defaults are the things I needed. Unless you have the same clock, you will probably need something different for some of the dimensions.

You could certainly print this laying flat, with the stub and clip rising up out of the bed. That's probably a mistake, though, since the stub and clip attachment points would be oriented with the filament layers and would be relatively weak. I printed mine standing on one of the thin edges. That requires supports. I used the auto-generated supports from PrusaSlicer, but I selected "support on build plate only". For my printer (Creality Ender 3v2), the bridging was good enough that I didn't need the other supports. (I did print with those other supports on some trials, and they were a pain to remove.) I used JAYO PLA+ filament with 15% infill. It seems to have the right springiness in the clip. 

I also figure that if it breaks I'll just print another one. I only have operate this clip once or twice a year when the battery runs down during a power outage.
