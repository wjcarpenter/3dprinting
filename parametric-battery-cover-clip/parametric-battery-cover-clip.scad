// The defaults are the dimensions of a board I was replacing, measured with
// calipers. The odds of yours being exactly the same are probably not good.
//
// The battery cover is all one piece, but it consists of 3 subparts.
// "cover" is the main chunk, approximately the same size as the batteries you're covering
// "stub" is the tiny fixed piece that wedges under something (at one end of "cover")
// "clip" is the flexible piece that you squeeze to remove the cover (at the other end of "cover")

/* [Cover (the big, main part)] */

// Cover length, does not include the clip nor the stub (X-axis)
cover_length = 55.50;                         // [0.01:0.01:9999]
// Cover width (Y-axis)
cover_width = 29.20;                          // [0.01:0.01:9999]
// Cover thickness (Z-axis)
cover_thickness = 2.55;                       // [0.01:0.01:9999]
// Cover clip notch width (Y-axis)
cover_clip_notch_width = 15.33;               // [0.01:0.01:9999]
// Cover clip notch depth (X-Axis)
cover_clip_notch_depth = 3;                   // [0.01:0.01:9999]

/* [Stub (the little stubby part)] */

// Stub width (Y-axis, like cover width)
stub_width = 7.33;                            // [0.01:0.01:9999]
// Stub length (X-axis, like cover length)
stub_length = 3;                              // [0.01:0.01:9999]
// Stub thickness (Z-axis, but going negative)
stub_thickness = 1.98;                        // [0.01:0.01:9999]
// Percentage of the stub length that overlaps with the cover (this will be the area where they two bond together)
stub_overlap_percent = 66;                    // [1:1:99]
// Stub shape as seen in cross-section; flat side is outboard
stub_shape = "T"; // [T:Triangular, R:Rectangular]

/* [Clip (the part that holds things in place)] */

// Clip width should be less than the cover clip notch width (Y-axis)
clip_width = 11.5;                            // [0.01:0.01:9999]
// If the entire clip is a ribbon, this is the thickness of the ribbon
clip_thickness = 1.5;                         // [0.01:0.01:9999]
// The length of the part of the clip attached to the cover (Z-axis, negative)
clip_inboard_length = 17;                     // [0.01:0.01:9999]
// The length of the other part of the V-shaped clip (it holds the neck) (Z-axis, negative)
clip_outboard_length = 19;                    // [0.01:0.01:9999]
// Angle in degrees where the clip turns back toward the light; it's the outboard panel that is tilted
clip_v_angle = 20;                            // [0.01:0.01:9999]
// Width needed for the neck to fit into some notch in the case (Y-axis)
clip_neck_width = 9;                          // [0.01:0.01:9999]
// Height needed for the neck to fit into some notch in the case (Z-axis)
clip_neck_height = 5.4;                       // [0.01:0.01:9999]
// Where the clip attaches to the cover, a quarter-circle fillet; use 0 to skip
clip_attachment_fillet_radius = 3;            // [0.01:0.01:9999]
// Where the two pieces of the clip meet, a cylinder added for strength; use 0 to skip
clip_v_cylinder_radius = 1.5;                 // [0.01:0.01:9999]

module __Customizer_Limit__ () {} // nothing below shows up in customizer

// The cover part is a rectangular solid with a rectangular solid notch removed
// from the clip end. The stub end is on the left (x=0) and the clip end is on the right.
module draw_cover_part() {
  difference() {
    cube([cover_length, cover_width, cover_thickness]);
    translate([cover_length - cover_clip_notch_depth, (cover_width - cover_clip_notch_width)/2, 0])
      cube([cover_clip_notch_depth, cover_clip_notch_width, cover_thickness]);
  }
}

module draw_stub_part() {
  if (search("T", stub_shape)) {
    translate([stub_length, stub_width, 0])
      rotate([270,0,180])
        linear_extrude(height=stub_width)
          polygon(points=[[0,0],[stub_length,0],[stub_length,stub_thickness]]);
  } else if (search("R", stub_shape)) {
    translate([0,0,-stub_thickness])
    cube([stub_length, stub_width, stub_thickness]);
  }
}

module draw_shoulder_notch() {
  cube([clip_thickness, (clip_width - clip_neck_width) / 2, clip_neck_height]);
}

module draw_clip_inboard_panel() {
  cube([clip_thickness, clip_width, clip_inboard_length]);
}

module draw_clip_outboard_panel() {
  difference() {
    cube([clip_thickness, clip_width, clip_outboard_length]);
    translate([0, 0, clip_outboard_length - clip_neck_height])
      draw_shoulder_notch();
    translate([0, clip_width - (clip_width - clip_neck_width) / 2, clip_outboard_length - clip_neck_height])
      draw_shoulder_notch();
  }
}

// In isolation, this puts the fillet in the right orientation
// for the clip attachment. It just has to be translated into
// the right position by the caller.
module draw_fillet(length, radius) {
  translate([-radius, length, -radius])
    rotate([90,0,0])
      difference() { 
        cube([radius, radius, length]); 
        cylinder(h=length, r=radius, $fn=25);
      }
}

module draw_clip_part() {
  draw_clip_inboard_panel();
  rotate(a=clip_v_angle, v=[0,1,0])
    draw_clip_outboard_panel();
  if (clip_v_cylinder_radius > 0) {
    translate([clip_v_cylinder_radius/2,clip_width,0])
      rotate([90,0,0])
      cylinder(h=clip_width, r=clip_v_cylinder_radius, $fn=25);
  }
  if (clip_attachment_fillet_radius > 0) {
    translate([0, 0, clip_inboard_length - cover_thickness])
      draw_fillet(clip_width, clip_attachment_fillet_radius);
  }
}

module draw_everything() {
  draw_cover_part();
  difference() {
    union() {
      translate([-stub_length*(100-stub_overlap_percent)/100,(cover_width-stub_width)/2,0])
	draw_stub_part();
      translate([cover_length - cover_clip_notch_depth,(cover_width-clip_width)/2, -clip_inboard_length+cover_thickness])
	draw_clip_part();
    }
    // The reason for this is to remove everything that might protrude above the cover.
    // This is simpler than fiddling about with all the individual measurements.
    translate([-cover_length, -cover_width, cover_thickness])
      // If you want to visualize what's being trimmed off, change "cube(" to "%cube(".
      cube([cover_length*3, cover_width*3, cover_thickness*3]);
  }
}

draw_everything();
