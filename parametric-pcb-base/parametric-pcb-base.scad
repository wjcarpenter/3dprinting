// This is a parametric design for a flat base to hold a PCB. It has 4
// posts for mounting the PCB and also to provide stand-offs from the
// base. If you 3D print this, don't forget to allow for shrinkage of
// your filament material, else it won't match the PCB mount
// holes. (For shrinkage, I recommend using precise dimensions here
// and enlarging the design by some percentage in your 3D print
// slicer.)

// In OpenSCAD preview mode, the parts are colored to make it easier
// to understand what this description is about.

// base: blue
// stub: red
// spike: green
// donut: orange
// grid holes: invisible :-)

// The base and mount hole positions can be taken directly from the
// kicad PCB layout. In kicad, the Y axis starts at the top of the
// page and increases toward the bottom. There's also generally an
// overall offset from a (0,0) origin. The SCAD layout is moved to a
// mostly (0,0) origin and is reversed with respect to the Y axis. You
// might not notice that since the board is likely to be symmetric
// both horizontally and vertically.

// For the PCB mount holes, there is a cylinder or square that holds
// the PCB away from the base. It's called the stub and has a slightly
// larger diameter or side than the mount holes that rest on
// it. Emerging out of the stub is another cylinder called the spike,
// which can be threaded. The spike is slightly smaller than the mount
// holes.

// The idea is that you can put the spikes through the mount holes
// so that the PCB rests on the stubs. You just need enough height for
// the stubs to keep any through-hole solder areas from touching the
// base. You just need enough height on the spike to pass through the
// PCB and be held in place with hot-melt glue or something. (If you
// were careful, you could melt down the top of the spikes to hold the
// PCB in place.) You can also use donuts slipped over the spikes to
// hold the PCB in place. The donuts and the spike can either be
// smooth or have matching threads.

// To use threads for the donuts and spikes, see the "use_threads"
// config item below. If you choose to use that, you must obtain the
// threads.scad library from https://github.com/rcolyer/threads-scad
// and put it in one of the locations described here:
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries (The
// simplest is to put it in the same directory as this file.)  If you
// don't use threads, you can simply comment the "use" line out.  (Or
// you can leave it as-is and ignore the warning given by OpenSCAD.)

// In 3D printed parts, there is always a worry about precision for
// parts that need to fit together. That's why the parameters below
// include separate items for adding or subtracting a little bit from
// some dimension. You may need to experiment a little bit on your 3D
// printer to get things to fit nicely. The default settings are what
// make my 3D printer happy, but yours may be different. To help with
// experimentation, see the "just_one_corner" config below.

// There is also a grid of rectangular holes in the base. You might
// use them to mount the base somewhere with cable ties or something
// else. If you don't want those holes in the base, just put a
// quantity of 0 for either X or Y (or both).

use <threads.scad>;

/* [Trial and error] */
// It can take some tuning to get the sizes just right, especially for threaded spikes and donuts. To save 3D print time, you can select this option to generate a small base, a single stub and spike, and a single donut.
just_one_corner = false;

/* [Base] */
// PCB rectangle, upper left X absolute position.
base_pcb_ul_x = 124.421;  // [0.001:0.001:999]
// PCB rectangle, upper left Y absolute position.
base_pcb_ul_y =  45.062;  // [0.001:0.001:999]
// PCB rectangle, lower right X absolute position.
base_pcb_lr_x = 195.421;  // [0.001:0.001:999]
// PCB rectangle, lower right Y absolute position.
base_pcb_lr_y = 144.062;  // [0.001:0.001:999]
// Extra padding for the base; this much on each edge.
base_x_indent = 5; // [0.1:0.1:9999]
// Extra padding for the base; this much on each edge.
base_y_indent = 5; // [0.1:0.1:9999]
// How thick to make the base.
base_thickness = 2.5; // [0.1:0.1:9999]

/* [PCB mount holes] */
// All mount holes are the same diameter.
mount_hole_diameter = 3.2; // [0.1:0.1:9]
// Upper left mount hole, X absolute center
mount_hole_ul_x = 128.427; // [0.001:0.001:999]
// Upper left mount hole, Y absolute center
mount_hole_ul_y =  49.076; // [0.001:0.001:999]
// Upper right mount hole, X absolute center
mount_hole_ur_x = 191.421; // [0.001:0.001:999]
// Upper right mount hole, Y absolute center
mount_hole_ur_y =  49.076; // [0.001:0.001:999]
// Lower left mount hole, X absolute center
mount_hole_ll_x = 128.427; // [0.001:0.001:999]
// Lower left mount hole, Y absolute center
mount_hole_ll_y = 140.062; // [0.001:0.001:999]
// Lower right mount hole, X absolute center
mount_hole_lr_x = 191.421; // [0.001:0.001:999]
// Lower right mount hole, Y absolute center
mount_hole_lr_y = 140.062; // [0.001:0.001:999]

/* [Stubs, spikes, and donuts] */
// The height of the stub does not include any part of the base.
stub_height  = 2.5; // [0.0:0.1:99]
// How much to add to the mount hole diameter to get the stub diameter.
stub_diameter_plus = 2.0; // [0.0:0.1:9]
// If you choose to use square donuts, you might like to use square stubs for aesthetic reasons.
use_square_stubs = false;
// The height of the spike does not include any part of the base or stub.
spike_height = 5.0; // [0.0:0.1:499]
// How much to substract from the mount hole diameter to get the spike diameter.
spike_diameter_minus = 0.1; // [0.0:0.1:9]
// Spike smooth extension to make it easier to engage the threading of the donut hole. Only added if you are using threads.
spike_extension_height = 2.5;  // [0.0:0.1:499]
// How much to substract from the spike diameter to get the spike extension diameter. For threaded spikes, you ware trying to find the inside thread diameter so the donut can pass over the extension. Use 0 to get a value calculated based on thread parameters.
spike_extension_diameter_minus = 0; // [0.0:0.1:9]
// Donut height should be less than or equal to the spike height. If you want to make it flush with the spike top, subtract the thickness of your PCB (typically 1.6mm).
donut_height = 3.4; // [0.0:0.1:499]
// Increase the donut hole diameter by this much relative to the spike diameter.
donut_hole_diameter_plus = 0.3; // [0.0:0.1:9]
// Donut quantity should be one for each spike, but you can make extras.
donut_quantity = 4;
// Use threads on the spike and the donut hole. If you choose this, see the note "use <threads.scad>" at the top of this file.
use_threads = true;
// Making the donuts square can make it a little easier to screw them on if they are threaded.
use_square_donuts = false;
// Thread pitch. Use 0 for standard metric thread pitches.
thread_pitch = 1.4; // [0.0:0.1:9]
// A sort of "flatness" of the threads. Higher numbers are flatter, so easier to use but less strong.
thread_tooth_angle = 50.0; // 0:1:100
// The height of the thread teeth. Use 0 to have a height the same as the thread_pitch value. You don't want it to be more than the thread_pitch value. If it's smaller, there will be flat ramps between threads, which can make it easier to use.
thread_tooth_height = 0.7; // [0.0:0.1:9]

/* [Grid holes] */
// X dimension of each tie grid hole
grid_hole_x = 3.5;  // [0.1:0.1:20]
// Y dimension of each tie grid hole
grid_hole_y = 3.5;  // [0.1:0.1:20]
// This many tie grid holes in the X direction
grid_hole_quantity_x = 9; // [0:1:50]
// This many tie grid holes in the Y direction
grid_hole_quantity_y = 14; // [0:1:99]
// Leave this much room on both ends of the X direction
grid_indent_x = 7; // [0.1:0.1:99]
// Leave this much room on both ends of the X direction
grid_indent_y = 7; // [0.1:0.1:99]

module __Customizer_Limit__ () {} // nothing below shows up in customizer

pcb_x_size = base_pcb_lr_x - base_pcb_ul_x;
pcb_y_size = base_pcb_lr_y - base_pcb_ul_y;
base_x_size = pcb_x_size + base_x_indent*2;
base_y_size = pcb_y_size + base_y_indent*2;
grid_x_size = pcb_x_size - (2 * grid_indent_x) - grid_hole_x;
grid_y_size = pcb_y_size - (2 * grid_indent_y) - grid_hole_y;
spike_diameter = mount_hole_diameter - spike_diameter_minus;
stub_diameter  = mount_hole_diameter + stub_diameter_plus;
donut_diameter = stub_diameter;
donut_hole_diameter = spike_diameter + donut_hole_diameter_plus;

// for just_one_corner
just_one_base_xy = stub_diameter * 2;
just_one_mount_hole_x = just_one_base_xy / 2;
just_one_mount_hole_y = just_one_base_xy / 2;

module draw_donut_outside() {
  if (use_square_donuts) {
    translate([-donut_diameter/2, -donut_diameter/2, 0])
      cube([donut_diameter, donut_diameter, donut_height]);  
  } else {
    cylinder(h=donut_height, r=donut_diameter/2, $fn=25);
  }
}
  
module draw_donuts() {
  donut_x = (just_one_corner ? (just_one_base_xy + donut_diameter) : (base_x_size + donut_diameter));
  dq = (just_one_corner ? (1) : (donut_quantity));
  donut_y_step = donut_diameter * 1.2;
  for (y = [1:dq]) {
    translate([donut_x, y * donut_y_step, 0])
      if (use_threads) {
        ScrewHole(donut_hole_diameter, donut_height, pitch=thread_pitch, tolerance=0, tooth_angle=thread_tooth_angle, tooth_height=thread_tooth_height)
          draw_donut_outside();
      } else {
        difference() {
          draw_donut_outside();
          cylinder(h=donut_height, r=donut_hole_diameter/2, $fn=25);
        }
      }
  }
}

module draw_grid_holes() {
  if (grid_hole_quantity_x > 0  &&  grid_hole_quantity_y > 0) {
    x_step = grid_x_size / (grid_hole_quantity_x - 1);
    y_step = grid_y_size / (grid_hole_quantity_y - 1);
    translate([grid_indent_x, grid_indent_y, 0])
      for (x = [0 : x_step : grid_x_size]) {
        for (y = [0 : y_step : grid_y_size]) {
          translate([x, y, 0])
            cube([grid_hole_x, grid_hole_y, base_thickness]);  
        }
      }
  }
}

module draw_base() {
  if (just_one_corner) {
    cube([just_one_base_xy, just_one_base_xy, base_thickness]);
  } else {
      difference() {
        translate([-base_x_indent, -base_y_indent, 0])
          cube([base_x_size, base_y_size, base_thickness]);
        draw_grid_holes();
    }
  }
}

module draw_one_spike() {
  translate([0, 0, base_thickness + stub_height]) {
    if (use_threads) {
      ScrewThread(spike_diameter, spike_height, pitch=thread_pitch, tolerance=0, tooth_angle=thread_tooth_angle, tooth_height=thread_tooth_height);
      translate([0, 0, spike_height])
        if (spike_extension_diameter_minus == 0) {
          // this is copied/modified from threads.scad
          pitch = (thread_pitch==0) ? ThreadPitch(spike_diameter) : thread_pitch;
          tooth_height = (thread_tooth_height==0) ? pitch : thread_tooth_height;
          tooth_angle = (thread_tooth_angle == 0) ? 30 : thread_tooth_angle;
          inner_diam = spike_diameter - tooth_height/tan(tooth_angle);
          cylinder(h=spike_extension_height, r=(inner_diam)/2.0, $fn=25);
        } else {
          cylinder(h=spike_extension_height, r=(spike_diameter - spike_extension_diameter_minus)/2.0, $fn=25);
        }
    } else {
      cylinder(h=spike_height, r=(spike_diameter)/2.0, $fn=25);
    }
  }
}

module draw_one_stub() {
  translate([0, 0, base_thickness])
    if (use_square_stubs) {
      translate([-stub_diameter/2, -stub_diameter/2, 0])
        cube([stub_diameter, stub_diameter, stub_height]);
    } else {
      cylinder(h=stub_height, r=(stub_diameter)/2.0, $fn=25);
    }
}

module draw_one_mount_hole() {
  color("red")
    draw_one_stub();
  color("green")
    draw_one_spike();
}

module draw_mount_holes() {
  if (just_one_corner) {
    translate([just_one_mount_hole_x, just_one_mount_hole_y, 0])
      draw_one_mount_hole();
  } else {
    translate([-base_pcb_ul_x, -base_pcb_ul_y, 0]) {
      translate([mount_hole_ul_x, mount_hole_ul_y, 0])
        draw_one_mount_hole();
      translate([mount_hole_ur_x, mount_hole_ur_y, 0])
        draw_one_mount_hole();
      translate([mount_hole_ll_x, mount_hole_ll_y, 0])
        draw_one_mount_hole();
      translate([mount_hole_lr_x, mount_hole_lr_y, 0])
        draw_one_mount_hole();
    }
  }
}

color("blue")
  draw_base();
draw_mount_holes();
color("orange")
  draw_donuts();
