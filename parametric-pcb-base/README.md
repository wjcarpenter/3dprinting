Parametric PCB base
===================

The master location for these files is
https://gitlab.com/wjcarpenter/3dprinting/-/tree/main/parametric-pcb-base
They are also available at https://www.thingiverse.com/thing:5713009

For a project I was working on, I was well on the way to designing a
custom enclosure. I did a lot of work on the base to hold the
PCB. When I was looking around for something to help with a detail of
the rest of the enclosure, I came across mrWheel's YAPP_Box project,
https://github.com/mrWheel/YAPP_Box, which does everything I was
planning, plus a lot more. So, I am not even using this design myself,
but some of the OpenSCAD things I figured out along the way
(especially threaded spikes and donuts) might be useful to someone in
some context.

This is a parametric design for a flat base to hold a PCB. It has 4
posts for mounting the PCB and also to provide stand-offs from the
base. If you 3D print this, don't forget to allow for shrinkage of
your filament material, else it won't match the PCB mount holes. (For
shrinkage, I recommend using precise dimensions here and enlarging the
design by some percentage in your 3D print slicer.)

In OpenSCAD preview mode, the parts are colored to make it easier
to understand what this description is about.

- base: blue
- stub: red
- spike: green
- donut: orange
- grid holes: invisible :-)

The base and mount hole positions can be taken directly from the kicad
PCB layout. In kicad, the Y axis starts at the top of the page and
increases toward the bottom. There's also generally an overall offset
from a (0,0) origin. The SCAD layout is moved to a mostly (0,0) origin
and is reversed with respect to the Y axis. You might not notice that
since the board is likely to be symmetric both horizontally and
vertically.

For the PCB mount holes, there is a cylinder or square that holds the
PCB away from the base. It's called the stub and has a slightly larger
diameter or side than the mount holes that rest on it. Emerging out of
the stub is another cylinder called the spike, which can be
threaded. The spike is slightly smaller than the mount holes.

The idea is that you can put the spikes through the mount holes so
that the PCB rests on the stubs. You just need enough height for the
stubs to keep any through-hole solder areas from touching the
base. You just need enough height on the spike to pass through the PCB
and be held in place with hot-melt glue or something. (If you were
careful, you could melt down the top of the spikes to hold the PCB in
place.) You can also use donuts slipped over the spikes to hold the
PCB in place. The donuts and the spike can either be smooth or have
matching threads.

To use threads for the donuts and spikes, see the "use_threads" config
item below. If you choose to use that, you must obtain the
threads.scad library from https://github.com/rcolyer/threads-scad and
put it in one of the locations described here:
https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries (The
simplest is to put it in the same directory as this file.)  If you
don't use threads, you can simply comment the "use" line out.  (Or you
can leave it as-is and ignore the warning given by OpenSCAD.)

In 3D printed parts, there is always a worry about precision for parts
that need to fit together. That's why the parameters below include
separate items for adding or subtracting a little bit from some
dimension. You may need to experiment a little bit on your 3D printer
to get things to fit nicely. The default settings are what make my 3D
printer happy, but yours may be different. To help with
experimentation, see the "just_one_corner" config below.

There is also a grid of rectangular holes in the base. You might use
them to mount the base somewhere with cable ties or something else. If
you don't want those holes in the base, just put a quantity of 0 for
either X or Y (or both).
