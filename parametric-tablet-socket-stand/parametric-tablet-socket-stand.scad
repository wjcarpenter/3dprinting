// OVERVIEW

// https://gitlab.com/wjcarpenter/3dprinting/-/tree/main/parametric-tablet-socket-stand

// This is a parametric design for a socket-like stand for a tablet or
// similar device. It consists of a rectangular base whose top side is
// at 0 in the XY plane (ie, the base is completely negative in the Z
// direction), the socket itself that is intended to hold the device
// by gravity and friction, a optional extra post on either or both
// sides of the socket (allowing you to clip something onto it),
// optional grooves/ridges on the back part of the base (in case you
// want to use some kind of supporting brace), and optional mounting
// holes in the base. Optional raised lips can be put on any or all
// sides of the base (for example, to keep your pencil from rolling
// onto the floor). As a separate piece, there is a clip-on brace. The
// defaults for everything are convenient for a Raspberry Pi 3B+
// bolted to a touchscreen that I needed to mount standing up.

// BASE AND SOCKET

// The base should be obvious. It's the big rectangular surface on
// which everything rests. The socket is where the device is
// inserted. It's a 4-sided rectangular box, where the back side can
// optionally be taller than the rest of the box. The socket gap is
// the hollowed out part of the box and should have the same size as
// the the device it will hold. The socket is automatically centered
// on the base's X axis.  You explicitly say how far to indent from
// the front of the base in the Y direction. The socket can also be
// leaned back at an angle. 90 degrees is straight up; 180 degress is
// flat on its back. Sensible values will be in that range. (If you
// want it to lean forward, you should instead just mentally reverse
// things so that a backward lean looks forwward when you physically
// turn it around.)

// MOUNTING HOLES AND CLIPPING POSTS

// The base can have mounting holes on the 4 corners. Mounting holes
// are optional, though it's all or none. The socket can have a
// clipping post on any or all of 4 positions. What's a clipping post?
// In my use case, I needed to attach a microphone with a clip
// intended to attach to clothing. One of those clipping posts is
// ideal for the orientation that is most common for that sort of
// clip. The clipping posts are optional and can be individually
// selected for each of the possible positions.

// CUTOUTS

// You can make some cutouts (rectangular solids) on any of
// the socket walls. That's useful in case your device has either
// something sticking out or something you need to be able to get to
// when it's nestled in the socket.  A wall's cutouts is a list of
// pairs, where each half of the pair is a list of 3 values.  The
// first item in the pair is a triple representing the x(width),
// y(height), z(thickness) size values for an OpenSCAD cube(), a
// rectangular solid.  The second item in the pair is a triple
// representing the x,y,z location for placement of that cube's
// [0,0,0] corner.  For the location, imagine you are standing inside
// the socket gap and looking at the applicable wall. The [0,0,0]
// point for relative placement of a cube is in the lower left and the
// inside surface of the wall. To clarify, if you hypothetically
// wanted to cut out a corner of the socket, you'd have to place a
// cutout of the wall thickness and height at a negative X position. I
// realize this sounds a little confusing, so it's probably easier if
// you just define some cutouts (or experiment with my defaults) and
// see what you get in OpenSCAD. That will probably make it
// clearer. Here is an example of how the settings are formatted. You
// can also look at the defaults for the variables cutout_front,
// cutouts_back, etc, below.
//
// [ [[l1,w1,h1],[x1,y1,z1]], [[l2,w2,h2],[x2,y2,z2]], [[l3,w3,h3],[x3,y3,z3]] ]

// GROOVES OR RIDGES

// You can make an optional series of grooves or ridges between the
// socket and the back edge of the base. This can be used for some
// kind of supporting buttress like you might have on a desktop
// picture frame. Such a buttress is not included in this design
// because it would depend on the device being supported; only
// included are the grooves or ridges that would hold it in place on
// the base end. You specify the length (centered on the base's X
// axis), thickness (or width if you prefer to think of it that way),
// and height of them. A positive height is a raised ridge; a negative
// height is a recessed groove. You also specify how many you
// want. The exact placements are calculated for you.  There is a gap
// between the back of the socket and the first groove/ridge, and
// there is a gap between the last groove/ridge and the back of the
// base. Both gaps are the same as the socket Y indent (well,
// approximately, since a groove/ridge is actually centered on the
// lines that mark those gap boundaries).

// LIPS

// (Sparks fly from her fingertips.) Any of the base's edges can have
// a raised lip. That's useful if you place small items or roll-away
// items on the base, in addition to the device you place in the
// socket. It might be a pen, pencil, stylus, or even a bird egg. The
// lip can help keep whatever it is from rolling away. Any lip you
// specify will be the entire length of the base edge, but you can say
// how thick and how high the lips should be.

// BRACE

// The optional brace is for propping something up. You might call it
// an easel back if you were talking about a picture frame. It has an
// (inverted) U-shaped part called the "clip" intended to fit snugly
// on top of whatever you are bracing. The clip has front, back, and
// top parts. Hooked to the back of the clip is a tail that slants
// away from it at an angle. The tail is the same width as the
// clip. The length of the tail piece is calculated, based on the
// angle and how high the clip rests. If you intend to put the bottom
// of the clip into a groove, you might want to add a little bit of
// length.

// COLORS

// In OpenSCAD preview mode, the parts are colored to make it easier
// to understand what this description is about. (In recent
// development builds of OpenSCAD, you can see the colors in the
// rendered view.)

//   base:           blue
//   socket:         red
//   clipping posts: orange
//   mounting holes: green
//   groves/ridges:  pink
//   lips:           yellow
//   tail:           green
//   clip:           blue

// 3D PRINTING

// If you make this on a 3D printer, you will probably need supports
// for any clipping posts. You might need supports for the back side
// of the socket, depending on how steep an angle you choose for
// leaning. Also keep in mind that the boundary between the base and
// the socket is likely to be in the direction with the least
// strength, so avoid hitting your mounted tablet with a baseball or
// cricket bat. The brace is rendered standing up in OpenSCAD to make
// it easy to see positioning relative the socket and base, but you
// will want to lay it on its side for 3D printing, both to avoid
// supports and to orient it for better strength.

// To generate the STLs from OpenSCAD, you should select the option to
// render just the socket and then just the brace even though you may
// want to view them together while manipulating in OpenSCAD.

/* [Parts to show] */
// Shown in preview but not rendered
render_socket_part = true;
// Shown in preview but not rendered
render_brace_part = true;

/* [Device Socket] */
// Socket Y indent from the front of the base
socket_y_indent = 10;  // [0.001:0.001:999]
// Size of the socket interior cutout, aka "the gap"
socket_gap_x_size = 184; // [0.1:0.1:9999]
// Size of the socket interior cutout, aka "the gap"
socket_gap_y_size = 20; // [0.1:0.1:9999]
// Socket Z height (if the socket is at 90 degrees to the base)
socket_z_height = 17; // [0.1:0.1:9999]
// Sometimes you want a little more on the back wall for support or want to shorten or remove side walls; this is added to socket Z height
socket_z_height_extra_for_back  = 25;
socket_z_height_extra_for_front =  0;
socket_z_height_extra_for_left  =  0;
socket_z_height_extra_for_right =  0;
// Wall thickness of the device socket
socket_wall_thickness = 3; //  [0.1:0.1:25]
// Socket angle (must be between 90 and 180, inclusive; 90 degrees is straight up from the base)
socket_angle = 90; // [90:1:180]

/* [Base] */
// Base X size
base_x_size = 194;  // [0.001:0.001:999]
// Base Y size
base_y_size = 125;  // [0.001:0.001:999]
// Base Z thickness
base_z_thickness = 3; // [0.1:0.1:9999]

/* [Groove/Ridges] */
// The long dimension, parallel to the base front and back
groove_ridge_x_size = 150; // [0.1:0.1:99]
// The thickness or width, perpendicular to the length
groove_ridge_y_size = 2.5; // [0.1:0.1:99]
// The depth or height; negative values for grooves, positive values for ridges
groove_ridge_z_size = 2.5; // [-99:0.1:99]
// The number of grooves or ridges, use 0 to omit
groove_ridge_count = 10; // [0:1:9999]

/* [Mounting holes] */
// Should mounting holes be rendered or omitted?
use_mounting_holes = true;
// All mounting holes are the same diameter.
mounting_hole_diameter = 3.2; // [0.1:0.1:9]
// Each mounting hole is this far from the base edge (X).
mounting_hole_x_indent = 5; // [0.001:0.001:999]
// Each mounting hole is this far from the base edge (Y)
mounting_hole_y_indent = 5; // [0.001:0.001:999]

/* [Base lips] */
// Left/right as seen from the front.
use_base_lip_left = false;
use_base_lip_right = false;
use_base_lip_front = true;
use_base_lip_back = false;
base_lip_thickness = 2.5; // [0.1:0.1:99]
base_lip_height = 4; // [0.1:0.1:99]

/* [Clipping posts] */
// Left/right as seen from the front.
use_clipping_post_left_front  = true;
use_clipping_post_right_front = false;
use_clipping_post_left_rear   = true;
use_clipping_post_right_rear  = false;
// All clipping posts have the same diameter
clipping_post_diameter = 4; // [0.1:0.1:25]
// Each clipping post center is this far from the socket edge (X)
clipping_post_x_indent = 4; // [0.001:0.001:999]
// Each clipping post center is this high above the base (Z)
clipping_post_z_indent = 13.5; // [0.001:0.001:999]
// The clipping post extends this far from the front or back wall
clipping_post_length = 5; // [0.001:0.001:999]

/* [Socket cutouts] */
// Relative to the inside walls of the socket
cutouts_front = [ [[8,50,2],[1.1,0,0]] , [[8,50,2],[174.9,0,0]] ];
// Placement origin is at the inside corner of the wall at the top of the base
cutouts_back  = [ [[23,100,5],[46,0,28]], [[10,100,5],[75,0,10]], [[32,100,5],[85,0,25]], [[12,100,5],[117,0,15]] ];
cutouts_left  = [];
cutouts_right = [];

/* [Brace] */
// The top side of the brace should rest at this height
brace_item_height = 110; // [0.1:0.1:999]
// The material thickness of the brace
brace_material_thickness = 3; // [0.1:0.1:99]
// The width of the top, front, and back clip parts
brace_material_width = 60; // [0.1:0.1:99]
// Length of the front and back faces of the brace
brace_front_back_internal_height = 5; // [0:0.1:90]
// Relative to the socket gap Y size (positive is larger, negative is smaller)
brace_clip_gap_delta = 2; // [-90:0.1:90]
// Angle of the tail relative to the clip back; 0 is straight down, 90 is flat out
brace_tail_angle = 31; // [0:0.1:90]
// In case you want to add (or subtract) a little bit of length for the tail
brace_tail_height_delta = 2.5; // [0:0.1:90]

/* [Colors] */
// For color names, see wikibooks.org/wiki/OpenSCAD_User_Manual/Transformations#color
color_for_base = "blue";
color_for_socket = "red";
color_for_socket_clipping_posts = "orange";
color_for_base_mounting_holes = "green";
color_for_base_grooves_or_ridges = "pink";
color_for_base_lips = "yellow";
color_for_brace_tail = "green";
color_for_brace_clip = "blue";

module __Customizer_Limit__ () {} // nothing below shows up in customizer

// Socket X indent from both sides of the base
socket_x_indent = (base_x_size - (socket_gap_x_size + 2*socket_wall_thickness)) / 2;

module construct_one_cutout(cutout) {
  size = cutout[0];
  width = size[0];
  height = size[1];
  thickness = size[2];
  location = cutout[1];
  translate(location)
    cube([width, thickness, height], center=false);
}

module construct_cutouts(cutouts) {
  for (cutout = cutouts) {
    construct_one_cutout(cutout);
  }
}

// Drawing this as if in the final upright degree position
module draw_socket_wall(side) {
  cutouts = (side == "left"  ? cutouts_left :
             side == "right" ? cutouts_right :
             side == "front" ? cutouts_front :
                               cutouts_back);
  height =  (side == "left"  ? socket_z_height + socket_z_height_extra_for_left :
             side == "right" ? socket_z_height + socket_z_height_extra_for_right :
             side == "front" ? socket_z_height + socket_z_height_extra_for_front :
                               socket_z_height + socket_z_height_extra_for_back);

  extra = (side == "back" ? socket_z_height_extra_for_back : 0);
  width = (side == "left" || side == "right") ? socket_gap_y_size : base_x_size - 2*(socket_x_indent + socket_wall_thickness);
  thickness = socket_wall_thickness;
  difference() {
    cube(size=[width, thickness, height], center=false);
    construct_cutouts(cutouts);
  }
}

module draw_one_socket_corner(corner) {
  height =  (corner == "left-front"  ? socket_z_height + min(socket_z_height_extra_for_left,  socket_z_height_extra_for_front) :
             corner == "left-back"   ? socket_z_height + min(socket_z_height_extra_for_left,  socket_z_height_extra_for_back) :
             corner == "right-front" ? socket_z_height + min(socket_z_height_extra_for_right, socket_z_height_extra_for_front) :
                                       socket_z_height + min(socket_z_height_extra_for_right, socket_z_height_extra_for_back));
  cube([socket_wall_thickness, socket_wall_thickness, height], center=false);
}

module draw_socket_corners() {
  translate([socket_x_indent,                                             socket_y_indent,                                             0])
    draw_one_socket_corner("left-front");
  translate([socket_x_indent,                                             socket_y_indent + socket_gap_y_size + socket_wall_thickness, 0])
    draw_one_socket_corner("left-back");
  translate([socket_x_indent + socket_gap_x_size + socket_wall_thickness, socket_y_indent,                                             0])
    draw_one_socket_corner("right-front");
  translate([socket_x_indent + socket_gap_x_size + socket_wall_thickness, socket_y_indent + socket_gap_y_size + socket_wall_thickness, 0])
    draw_one_socket_corner("right-back");
}

module draw_socket_surround() {
  // the 4 walls, and then the corners
  color(color_for_socket) {
    translate([socket_x_indent + socket_wall_thickness, socket_y_indent + socket_wall_thickness, 0])
      rotate(a=90, v=[0,0,1])
      draw_socket_wall("left");
    translate([base_x_size - socket_x_indent - socket_wall_thickness, socket_y_indent + socket_gap_y_size + socket_wall_thickness, 0])
      rotate(a=270, v=[0,0,1])
        draw_socket_wall("right");
    translate([socket_x_indent + socket_wall_thickness + socket_gap_x_size, socket_y_indent + socket_wall_thickness, 0])
      rotate(a=180, v=[0,0,1])
        draw_socket_wall("front");
    translate([socket_x_indent + socket_wall_thickness, socket_y_indent + socket_gap_y_size + socket_wall_thickness, 0])
      rotate(a=0, v=[0,0,1])
        draw_socket_wall("back");
    draw_socket_corners();
  }
  // attach the clipping posts
  color(color_for_socket_clipping_posts) {
    if (use_clipping_post_left_front)
      translate([socket_x_indent + clipping_post_x_indent, socket_y_indent, clipping_post_z_indent])
        draw_one_clipping_post();
    if (use_clipping_post_right_front)
      translate([base_x_size - (socket_x_indent + clipping_post_x_indent), socket_y_indent, clipping_post_z_indent])
        draw_one_clipping_post();
    if (use_clipping_post_left_rear)
      translate([socket_x_indent + clipping_post_x_indent, socket_y_indent + 2*socket_wall_thickness + socket_gap_y_size + clipping_post_length, clipping_post_z_indent])
        draw_one_clipping_post();
    if (use_clipping_post_right_rear)
      translate([base_x_size - (socket_x_indent + clipping_post_x_indent), socket_y_indent + 2*socket_wall_thickness + socket_gap_y_size + clipping_post_length, clipping_post_z_indent])
        draw_one_clipping_post();
  }
}

module draw_socket_with_clipping_posts() {
  difference() {
    rotate(a=-(socket_angle-90), v=[1,0,0])
      draw_socket_surround();
    translate([0, 0, -(socket_gap_y_size + 2*socket_wall_thickness)])
      cube([base_x_size, base_y_size, (socket_gap_y_size + 2*socket_wall_thickness)]);
  }
}

module draw_base() {
  translate([0, 0, -base_z_thickness])
    cube([base_x_size, base_y_size, base_z_thickness]);
}

module draw_base_lips() {
  // left
  if (use_base_lip_left) {
    translate([base_lip_thickness,0,0])
      rotate(a=90, v=[0,0,1])
        cube([base_y_size, base_lip_thickness, base_lip_height]);
  }
  // right
  if (use_base_lip_right) {
    translate([base_x_size,0,0])
      rotate(a=90, v=[0,0,1])
        cube([base_y_size, base_lip_thickness, base_lip_height]);
  }
  // front
  if (use_base_lip_front) {
    translate([0,0,0])
      rotate(a=0, v=[0,0,1])
        cube([base_x_size, base_lip_thickness, base_lip_height]);
  }
  // back
  if (use_base_lip_back) {
    translate([0,base_y_size-base_lip_thickness,0])
      rotate(a=0, v=[0,0,1])
        cube([base_x_size, base_lip_thickness, base_lip_height]);
  }
}

module draw_one_clipping_post() {
    rotate(a=90, v=[1,0,0])
      cylinder(h=clipping_post_length, r=clipping_post_diameter/2, $fn=25);
}

module draw_one_mounting_hole() {
  cylinder(h=base_z_thickness, r=(mounting_hole_diameter)/2.0, $fn=25);
}

module draw_mounting_holes() {
  if (use_mounting_holes) {
    translate([mounting_hole_x_indent,               mounting_hole_y_indent,               -base_z_thickness])
      draw_one_mounting_hole();
    translate([mounting_hole_x_indent,               base_y_size - mounting_hole_y_indent, -base_z_thickness])
      draw_one_mounting_hole();
    translate([base_x_size - mounting_hole_x_indent, mounting_hole_y_indent,               -base_z_thickness])
      draw_one_mounting_hole();
    translate([base_x_size - mounting_hole_x_indent, base_y_size - mounting_hole_y_indent, -base_z_thickness])
      draw_one_mounting_hole();
  }
}

module draw_gooves_ridges(positive) {
  start_y = 2*socket_y_indent + socket_gap_y_size + 2*socket_wall_thickness;
  stop_y  = base_y_size - socket_y_indent;
  separation = (stop_y - start_y) / (groove_ridge_count - 1);
  groove_x_indent = (base_x_size - groove_ridge_x_size) / 2;
  half_y = groove_ridge_y_size / 2;
  for (gr = [1:1:groove_ridge_count]) {
    if (positive && groove_ridge_z_size > 0) {
      translate([groove_x_indent, start_y - half_y + (gr - 1) * separation, 0])
        cube([groove_ridge_x_size, groove_ridge_y_size, groove_ridge_z_size], center=false);
    }
    else if (!positive && groove_ridge_z_size < 0) {
      translate([groove_x_indent, start_y - half_y + (gr - 1) * separation, groove_ridge_z_size])
        cube([groove_ridge_x_size, groove_ridge_y_size, -groove_ridge_z_size], center=false);
    }
  }
}

pc0 = [0, 0];
pc1 = [pc0[0], pc0[1] + brace_material_thickness + brace_front_back_internal_height];
pc2 = [pc1[0] + socket_gap_y_size + brace_clip_gap_delta + 2*brace_material_thickness, pc1[1]];
pc3 = [pc2[0], pc0[1]];
pc4 = [pc3[0] - brace_material_thickness, pc3[1]];
pc5 = [pc4[0], pc4[1] + brace_front_back_internal_height];
pc6 = [pc0[0] + brace_material_thickness, pc5[1]];
pc7 = [pc6[0], pc0[1]];
points_for_clip = [pc0, pc1, pc2, pc3, pc4, pc5, pc6, pc7];

pt0 = pc3;
pt1 = [pt0[0] + (brace_item_height - brace_front_back_internal_height)/tan(90 - brace_tail_angle), (brace_front_back_internal_height - brace_item_height) - brace_tail_height_delta];
pt2 = [pt1[0] - brace_material_thickness, pt1[1]];
pt3 = pc4;
points_for_tail = [pt0, pt1, pt2, pt3];

module draw_brace_parts() {
  color(color_for_brace_clip)
    linear_extrude(height=brace_material_width)
      polygon(points_for_clip);
  color(color_for_brace_tail)
    linear_extrude(height=brace_material_width)
      polygon(points_for_tail);
}

module render_brace_part() {
  translate([(base_x_size - brace_material_width)/2, socket_y_indent + socket_wall_thickness -  brace_material_thickness, brace_item_height - brace_front_back_internal_height])
    rotate([90, 0, 90])
      draw_brace_parts();
}

module render_socket_part() {
  difference() {
    union() {
      color(color_for_base)
        draw_base();
      color(color_for_base_lips)
        draw_base_lips();
      draw_socket_with_clipping_posts();
      color(color_for_base_grooves_or_ridges)
        draw_gooves_ridges(true);
    }
    color(color_for_base_grooves_or_ridges)
      draw_gooves_ridges(false);
    color(color_for_base_mounting_holes)
      draw_mounting_holes();
  }
}

if (render_socket_part) {
  render_socket_part();
} else {
  %render_socket_part();
}
if (render_brace_part) {
  render_brace_part();
} else {
  %render_brace_part();
}
