// 2 piece case for PIR sensor SR501
// PCB volume size 33x25x14
// created by jegb 2014
// CC BY-NC-SA-3.0

// remixed by WJCarpenter, June 2022

/* [Rendering choices] */
what_to_render = "BT"; // [B:render bottom half, T:render top half, BT: render both halves - side by side, A: render both halves - assembled, D:render DXF]
render_top = search("T", what_to_render);
render_bottom = search("B", what_to_render);
render_assembled_case = search("A", what_to_render);
render_dxf = search("D", what_to_render);

// If you render both top and bottom, they will have this much space between them. Make it wider if you intend to use a brim when you slice for 3D printing
render_gap = 10; // [0:1:50]
// Higher numbers are smoother round edges but also increase rendering time; reduce it to go faster while designing
smooth_curviness = 25;

/* [PCB and case dimensions] */
// The defaults are the dimensions of a board I had on hand, measured with
// calipers. The odds of yours being exactly the same are probably not good.
// In fact, the dimensions of other boards that I have are probably slightly
// different.


// Length of the short side of the PCB (the sides with screw holes)
pcb_short_side_length = 24.25; // [0:0.1:50]
// Inside short side length
case_inside_short_side_length = 24.25; // [0:0.1:100]
// Length of the long side of the PCB
pcb_long_side_length = 32.42;  // [0:0.1:50]
// Inside short side length
case_inside_long_side_length = 32.42; // [0:0.1:100]

// Sides of the case
wall_thickness = 2; // [0:0.1:10]
case_outside_long_side_length  = case_inside_long_side_length  + 2*wall_thickness;
case_outside_short_side_length = case_inside_short_side_length + 2*wall_thickness;
// The floor of the bottom half
bottom_floor_thickness = 1; // [0:0.1:10]
// The ceiling of the top half
top_ceiling_thickness = 1; // [0:0.1:10]
// Inside height of bottom case half
case_inside_height_bottom = 8; // [0:0.1:100]
case_outside_height_bottom = case_inside_height_bottom + bottom_floor_thickness;
// Inside height of top case half (not counting the lip, if used)
case_inside_height_top = 8; // [0:0.1:100]
case_outside_height_top = case_inside_height_top + top_ceiling_thickness;
// Inside corners are square, but outside corners are rounded.
outside_corner_radius = 3; // [0:1:10]

/* [PCB standoffs and Edge clamps, case mounting ears and screw holes] */

// "L" means corner, "X" means short side centers (two small PCB holes there)
// Center standoffs are aligned with PCB holes for screws or glue; corner standoffs are at the 4 corners of the PCB
which_PCB_standoffs_to_use = "LX"; // [0:none, L:corners, X:centered on short sides, LX:both centered and corners]
do_center_standoffs = search("X", which_PCB_standoffs_to_use);
do_corner_standoffs = search("L", which_PCB_standoffs_to_use);

// Center standoffs are squares with sides this long
center_standoff_size = 3; // [0:0.1:10]
// If you use both center and corner stand-offs, you want them to be the same height
center_standoff_height = 2; // [0:0.1:10]
// Corner standoffs are squares with sides this long
corner_standoff_size = 3; // [0:0.1:10]
// If you use both center and corner stand-offs, you want them to be the same height
corner_standoff_height = 2; // [0:0.1:10]

// Edge clamps to hold the sides of the PCB (requires pcb short or long padding)
which_PCB_edge_clamps_to_use = "0"; // [0:none, L:PCB long edges, S:PCB short edges, LS:both long and short PCB edges]
do_long_edge_clamps  = search("L", which_PCB_edge_clamps_to_use);
do_short_edge_clamps = search("S", which_PCB_edge_clamps_to_use);
// Edge clamp width; length is the same as the PCB side it clamps
edge_clamp_width = 4; // [0:0.1:10]
// If you use both standoffs and edge clamps, you want the edge clamps to be taller
edge_clamp_height = 5; // [0:0.1:10]

// Which case halves get mounting ears? Centered on the short side of the case half
which_case_mounting_ears_to_use = "B"; // [0:none, B:bottom case half, T:top case half, BT:both case halves]
do_ears_on_bottom = search("B", which_case_mounting_ears_to_use);
do_ears_on_top    = search("T", which_case_mounting_ears_to_use);

ear_height = 2; // [0:0.1:10]
ear_length = 18; // [0:0.1:50]
ear_width = 6; // [0:0.1:25]
// Slots in ears for mounting screws
which_case_mounting_ears_have_screw_slots = "BT"; // [0:none, B:bottom case half ears, T:top case half ears, BT:both case half ears]
do_ear_slots_on_bottom = search("B", which_case_mounting_ears_have_screw_slots);
do_ear_slots_on_top    = search("T", which_case_mounting_ears_have_screw_slots);

ear_slot_width = 3; // [0:0.1:25]
ear_slot_length = 5; // [0:0.1:50]
// Allows a little extra room on the case side of the slot so the screw head is not crowded; does not change the ear size
ear_slot_case_offset = 1; // [0:0.1:10]

// Screw holes in the bottom case floor, at the corners
make_inside_screw_holes = false;
screw_hole_radius = 3; // [0:0.1:10]
// Screw holes will be placed in corners at this distance from the walls, to allow room for screw heads
screw_hole_case_offset = 2; // [0:0.1:10]

/* [Miscellaneous cut-outs] */
// Circular versus rectangular
PIR_dome_cutout_is_round = true;
// Radius of dome, for round cut-out
PIR_dome_radius = 11.5; // [0:0.1:25]
// Length that parallels case short side, for rectangular dome cut-out
PIR_short_side_length = 11.56; // [0:0.1:25]
// Length that parallels case long side, for rectangular dome cut-out
PIR_long_side_length = 11.52; // [0:0.1:25]

// You can remove part or all of the case bottom floor as a centered rectangle
floor_floor_cutout = false;
floor_cutout_long_edge  = 32.42;  // [0:0.1:100]
floor_cutout_short_edge = 24.25; // [0:0.1:100]

// Rectangular or semi-circle cord entry on case long side; you might need a support for the rectangle for 3D printing
make_cord_cutout = "0"; // [0:none, B:bottom case half, T:top case half, BT:both case halves]
do_cord_cutout_on_bottom = search("B", make_cord_cutout);
do_cord_cutout_on_top    = search("T", make_cord_cutout);
// Can be either a semi-circle or a rectangle
cord_cutout_is_semicircle = true;
// Radius for round cord entry cutout
cord_cutout_radius = 5; // [0:0.1:20]
// Length of the rectangular cord entry cut-out
cord_cutout_width = 6; // [0:0.1:20]
// Height of the rectangular cord entry cut-out
cord_cutout_height = 4; // [0:0.1:20]

// Small rectangle in case bottom floor for 3-pin Dupont jumper wire; size and location are hard-coded
make_jumper_cutout = true;

/* [Lips for joining halves, coin notches to get them apart] */
use_lip_to_join_case_halves = true;
lip_thickness = 1; // [0:0.1:5]
lip_height = 2; // [0:0.1:5]
// The top lip height and thickness will be this much smaller than the bottom lip recess for a looser fit
lip_delta = 0; // [0:0.01:10]
// You can make the lip on the top half a little more flexible by removing this much at each end of each edge; should be more than lip thickness
lip_corner_trim = 2; // [0:0.1:5]

// Coin notches on the top half are for prying the two halves apart
coin_notch_on_near_short_side = false;
// Near means on the axis in layout; far means the opposite side, away from the axis
coin_notch_on_near_long_side = true;
coin_notch_on_far_short_side = true;
coin_notch_on_far_long_side = false;
coin_notch_length = 10; // [0:0.1:25]
// Some common coin thicknesses: US dime = 1.35mm, US penny = 1.52mm, Euro 1/2/5 cents = 1.67mm
coin_notch_height = 1.53; // [0:0.01:5]
coin_notch_depth = 1; // [0:0.01:5]


module __Customizer_Limit__ () {} // nothing below shows up in customizer

module create_subtractor_for_rounded_corners(height,radius) {
	difference(){ 
		cube([radius+0.1, radius+0.1, height]); 
		cylinder(h=height, r=radius+0.1, $fn=smooth_curviness);
	} //+0.1 stops ghost edges
}

module draw_rounded_corners(is_bottom) {
	o_height = (is_bottom ? case_outside_height_bottom : case_outside_height_top);
	translate([case_outside_short_side_length - outside_corner_radius, case_outside_long_side_length - outside_corner_radius])
		rotate(0)
			create_subtractor_for_rounded_corners(o_height, outside_corner_radius); 
	translate([outside_corner_radius, case_outside_long_side_length - outside_corner_radius]) 
		rotate(90) 
			create_subtractor_for_rounded_corners(o_height, outside_corner_radius);
	translate([outside_corner_radius,outside_corner_radius]) 
		rotate(180) 
			create_subtractor_for_rounded_corners(o_height, outside_corner_radius);
	translate([case_outside_short_side_length - outside_corner_radius, outside_corner_radius]) 
		rotate(270) 
			create_subtractor_for_rounded_corners(o_height, outside_corner_radius);
}

module draw_one_center_standoff(is_bottom=false) {
	cube([center_standoff_size, center_standoff_size, center_standoff_height]);
}
module draw_one_corner_standoff(is_bottom=false) {
	cube([corner_standoff_size, corner_standoff_size, corner_standoff_height]);
}

module draw_standoffs(is_bottom=false) {
	standoff_z = top_ceiling_thickness;
	if (do_center_standoffs) {
		center_x = (case_outside_short_side_length - center_standoff_size)/2;
		south_y  = (case_outside_long_side_length - pcb_long_side_length)/2;
		north_y  = (case_outside_long_side_length + pcb_long_side_length)/2 - center_standoff_size;
		translate([center_x, south_y, standoff_z]) 
			draw_one_center_standoff();
		translate([center_x, north_y, standoff_z])
			draw_one_center_standoff();
	}
	if (do_corner_standoffs) {
		west_x  = (case_outside_short_side_length - pcb_short_side_length)/2;
		east_x  = (case_outside_short_side_length + pcb_short_side_length)/2 - corner_standoff_size;
		south_y = (case_outside_long_side_length  - pcb_long_side_length)/2;
		north_y = (case_outside_long_side_length  + pcb_long_side_length)/2  - corner_standoff_size;
		translate([west_x, south_y, standoff_z])
			draw_one_corner_standoff();
		translate([east_x, south_y, standoff_z]) 
			draw_one_corner_standoff();
		translate([west_x, north_y, standoff_z])
			draw_one_corner_standoff();
		translate([east_x, north_y, standoff_z])
			draw_one_corner_standoff();
	}
}

module draw_one_edge_clamp(is_short_side, is_bottom=false) {
	// Unlike many other shapes, these are centered
	if (is_short_side) {
		extra_for_corners = (do_short_edge_clamps && do_long_edge_clamps) ? (2 * edge_clamp_width) : 0;
		cube([pcb_short_side_length + extra_for_corners, edge_clamp_width, edge_clamp_height], center = true);
	} else {
		cube([edge_clamp_width, pcb_long_side_length, edge_clamp_height], center = true);
	}
}

module draw_edge_clamps(is_bottom=false) {
	clamp_z = top_ceiling_thickness + edge_clamp_height/2;
	if (do_long_edge_clamps) {
		west_x = (case_outside_short_side_length - pcb_short_side_length - edge_clamp_width) / 2;
		east_x = (case_outside_short_side_length + pcb_short_side_length + edge_clamp_width) / 2;
		translate([west_x, case_outside_long_side_length/2, clamp_z]) 
			draw_one_edge_clamp(is_short_side=false);
		translate([east_x, case_outside_long_side_length/2, clamp_z]) 
			draw_one_edge_clamp(is_short_side=false);
	}
	if (do_short_edge_clamps) {
		south_y = (case_outside_long_side_length - pcb_long_side_length - edge_clamp_width) / 2;
		north_y = (case_outside_long_side_length + pcb_long_side_length + edge_clamp_width) / 2;
		translate([case_outside_short_side_length/2, south_y, clamp_z]) 
			draw_one_edge_clamp(is_short_side=true);
		translate([case_outside_short_side_length/2, north_y, clamp_z])
			draw_one_edge_clamp(is_short_side=true);
	}
}

module draw_cord_cutout(is_bottom) {
	// cut-out shape is centered
	tz = is_bottom ? bottom_floor_thickness : case_outside_height_top;
	tx = case_outside_short_side_length - (wall_thickness/2);
	ty = case_outside_long_side_length / 2;
	if ((is_bottom  &&  do_cord_cutout_on_bottom)  ||  (!is_bottom  &&  do_cord_cutout_on_top)) {
		translate([tx,ty,tz])
			if (cord_cutout_is_semicircle) {
				rotate(a=90, v=[0,1,0])
					cylinder(h=wall_thickness, r=cord_cutout_radius, center=true);
			} else {
				cube([wall_thickness, cord_cutout_width, 2*cord_cutout_height], center=true);
			}
	}
}  

module draw_one_lip_trim(is_bottom=false, lh) {
	cube([lip_corner_trim, lip_corner_trim, lh]);
}

module draw_lip_trims(is_bottom=false, lh, lw) {
	c_trim = ((!is_bottom)  &&  (lip_corner_trim > 0));
	if (c_trim) {
		translate([0,0,0])
			draw_one_lip_trim(is_bottom, lh=lh);
		translate([case_inside_short_side_length + 2*lw - lip_corner_trim, 0, 0])
			draw_one_lip_trim(is_bottom, lh=lh);
		translate([0, case_inside_long_side_length + 2*lw - lip_corner_trim, 0])
			draw_one_lip_trim(is_bottom, lh=lh);
		translate([case_inside_short_side_length + 2*lw - lip_corner_trim, case_inside_long_side_length + 2*lw - lip_corner_trim, 0])
			draw_one_lip_trim(is_bottom, lh=lh);
	}
}

module draw_lip(is_bottom) {
	o_height = is_bottom ? case_outside_height_bottom : case_outside_height_top;
	if (use_lip_to_join_case_halves) {
		lw = (is_bottom ? lip_thickness : lip_thickness  - lip_delta);
		lh = (is_bottom ? lip_height    : lip_height - lip_delta);
		difference(){
			cube([case_inside_short_side_length + 2*lw, case_inside_long_side_length + 2*lw, lh]);
			translate([lw, lw, 0]) 
				cube([case_inside_short_side_length, case_inside_long_side_length, lh]);
			draw_lip_trims(is_bottom, lh=lh, lw=lw);
		}
	}
}

module draw_PIR_dome_cutout(is_bottom=false) {
	if (PIR_dome_cutout_is_round) {
		translate([case_outside_short_side_length/2, case_outside_long_side_length/2, 0]) 
			cylinder(h=top_ceiling_thickness, r=PIR_dome_radius, $fn=smooth_curviness);
	} else {
		translate([case_outside_short_side_length/2 - PIR_dome_radius, case_outside_long_side_length/2 - PIR_dome_radius, 0]) 
			cube([2*PIR_dome_radius, 2*PIR_dome_radius, top_ceiling_thickness]);
	}
}

module draw_one_coin_notch(is_bottom=false, is_short_side=false) {
	cn_x = (is_short_side ? coin_notch_length : coin_notch_depth);
	cn_y = (is_short_side ? coin_notch_depth  : coin_notch_length);
	cube ([cn_x, cn_y, coin_notch_height]);
}

module draw_coin_notches(is_bottom=false) {
	coin_notch_z = case_outside_height_top - coin_notch_height;
	if (coin_notch_on_near_long_side) {
		translate ([0, (case_outside_long_side_length - coin_notch_length)/2, coin_notch_z]) 
			draw_one_coin_notch(is_bottom, is_short_side=false);
	}
	if (coin_notch_on_near_short_side) {
		translate ([(case_outside_short_side_length - coin_notch_length)/2, 0, coin_notch_z]) 
		  	draw_one_coin_notch(is_bottom, is_short_side=true);
	}
	if (coin_notch_on_far_long_side) {
		translate ([case_outside_short_side_length - coin_notch_depth, (case_outside_long_side_length - coin_notch_length)/2, coin_notch_z]) 
		  	draw_one_coin_notch(is_bottom, is_short_side=false);
	}
	if (coin_notch_on_far_short_side) {
		translate ([(case_outside_short_side_length - coin_notch_length)/2, case_outside_long_side_length - coin_notch_depth, coin_notch_z]) 
			draw_one_coin_notch(is_bottom, is_short_side=true);
	}
}

module draw_one_screw_hole(is_bottom=true) {
	cylinder(h=top_ceiling_thickness, r=screw_hole_radius, $fn=smooth_curviness);
}

module draw_screw_holes(is_bottom=true) {
	if (make_inside_screw_holes) {
		sh_or = screw_hole_case_offset + screw_hole_radius;
		translate([wall_thickness + sh_or, wall_thickness + sh_or, 0])
			draw_one_screw_hole(is_bottom);
		translate([case_outside_short_side_length - wall_thickness - sh_or, wall_thickness + sh_or, 0])
			draw_one_screw_hole(is_bottom);
		translate([wall_thickness + sh_or, case_outside_long_side_length - wall_thickness - sh_or, 0])
			draw_one_screw_hole(is_bottom);
		translate([case_outside_short_side_length - wall_thickness - sh_or, case_outside_long_side_length - wall_thickness - sh_or, 0])
			draw_one_screw_hole(is_bottom);
	}
}

module draw_one_ear(is_bottom) {
	// cylinders here are centered
	ear_corner_radius = 3;
	difference() {
		hull() {
			translate([0, 0, 0])
				cube([ear_length, ear_width - ear_corner_radius, ear_height]);
			translate([ear_corner_radius, ear_width - ear_corner_radius, ear_height/2])
				cylinder(h=ear_height, r=ear_corner_radius, center=true, $fn=smooth_curviness);
			translate([ear_length - ear_corner_radius, ear_width - ear_corner_radius, ear_height/2])
				cylinder(h=ear_height, r=ear_corner_radius, center=true, $fn=smooth_curviness);
		}
		if ((is_bottom  &&  do_ear_slots_on_bottom)  ||  (!is_bottom  &&  do_ear_slots_on_top)) {
			ear_slot_radius = ear_slot_width / 2;
			translate([(ear_length - ear_slot_length )/2, ear_width/2 + ear_slot_case_offset, ear_height/2]) 
				hull() {
					translate([ear_slot_radius, 0, 0])
						cylinder(h=ear_height, r=ear_slot_radius, $fn=smooth_curviness, center=true);
					translate([ear_slot_length - ear_slot_radius, 0, 0])
						cylinder(h=ear_height, r=ear_slot_radius, $fn=smooth_curviness, center=true);
				}
		}
	}
}

module draw_ears(is_bottom){
	tz = is_bottom ? 0 : (case_outside_height_top - ear_height);
	if ((is_bottom  &&  do_ears_on_bottom)  ||  (!is_bottom  &&  do_ears_on_top)) {
		translate([(case_outside_short_side_length - ear_length)/2, 0, tz])
			mirror([0,1,0])
				draw_one_ear(is_bottom);
		translate([(case_outside_short_side_length - ear_length)/2, case_outside_long_side_length, tz])
			draw_one_ear(is_bottom);
	}
} 

module draw_case_bottom(){
	difference(){
		union(){
			cube ([case_outside_short_side_length, case_outside_long_side_length, case_outside_height_bottom]);
			draw_ears(is_bottom=true);
		}
		translate ([wall_thickness, wall_thickness, bottom_floor_thickness]) 
			cube([case_inside_short_side_length, case_inside_long_side_length, case_inside_height_bottom]);

                draw_rounded_corners(is_bottom=true);
		draw_screw_holes(is_bottom=true);
                if (floor_floor_cutout) {
			translate([case_outside_short_side_length/2, case_outside_long_side_length/2, 0])
				cube([floor_cutout_short_edge, floor_cutout_long_edge, 2*bottom_floor_thickness], center = true); 
			
		}
		draw_cord_cutout(is_bottom=true);
		if (make_jumper_cutout) {
			// pass-through dupont plug 8x3x6
			translate([(case_outside_short_side_length - pcb_short_side_length)/2 + 2, case_outside_long_side_length/2, 0])
				cube([3, 8, 2*bottom_floor_thickness], center = true); 
		}
		if (use_lip_to_join_case_halves) {
			translate([wall_thickness - lip_thickness, wall_thickness - lip_thickness, case_outside_height_bottom - lip_height]) 
				draw_lip(is_bottom=true);
		}
	}// difference
}

module draw_case_top(){
	difference() {
		union() {
			cube ([case_outside_short_side_length, case_outside_long_side_length, case_outside_height_top]);
			draw_ears(is_bottom=false);
		}
		// empty out central cavity
		translate ([wall_thickness, wall_thickness, top_ceiling_thickness]) 
			cube ([case_inside_short_side_length, case_inside_long_side_length, case_inside_height_top]);
		draw_rounded_corners(is_bottom=false);
		draw_PIR_dome_cutout(is_bottom=false);
		draw_coin_notches(is_bottom=false);
		draw_cord_cutout(is_bottom=false);
	}

        draw_standoffs(is_bottom=false);
        draw_edge_clamps(is_bottom=false);

	if (use_lip_to_join_case_halves) {
		translate([wall_thickness - lip_thickness + lip_delta, wall_thickness - lip_thickness + lip_delta, case_outside_height_top]) 
			draw_lip(is_bottom=false);
	}

}

if (render_assembled_case) {
	translate([case_outside_short_side_length, 0 ,case_outside_height_bottom+case_outside_height_top])
		rotate (a=180, v=[0,1,0]) 
			color ("orange") draw_case_top(); 
        color("cyan")
		draw_case_bottom();
}
if (render_dxf) {
	projection (cut=true) 
		translate([case_outside_short_side_length + render_gap, 0, -6.5]) 
			draw_case_top();
	projection (cut=true) 
		translate ([0,0,-6.5]) 
			draw_case_bottom();
}
if (render_top) {
	color("orange")
		if (render_bottom) {
			translate([case_outside_short_side_length + render_gap, 0, 0])
				draw_case_top();
		} else {
			draw_case_top();
		}
}
if (render_bottom) {
	color("cyan")
		draw_case_bottom();
}
