// https://hackaday.io/project/192719-calling-for-hot-water
// Enclosure for D1 Mini with relay board.

// Template from:
//-----------------------------------------------------------------------
// Yet Another Parameterized Projectbox generator
// See https://mrwheel-docs.gitbook.io/yappgenerator_en/ for
// documentation and a download pointer.
//-----------------------------------------------------------------------

// These "local" variables have to come before the "include" statement.
// The "hbrb_" numbers are sort of fundamental constants of mine.

// This file can be used to make an enclosure for the D1 Mini with relay shield.
// It can also be used to make an enclosure for a simple pushbutton for hooking
// directly to the water heater HotButton terminals. Which one you get is controlled
// by this boolean.
hbrb_relay_box = true;

hbrb_button_diameter = 16.75; // allow for shrinkage
hbrb_button_height = 20;

hbrb_bottom_zsize = hbrb_relay_box ? 14 : 10;
hbrb_lid_zsize = hbrb_relay_box ? 22 : (hbrb_button_height - hbrb_bottom_zsize);

hbrb_magnet_thickness = 3.0;
hbrb_magnet_diameter = 8 + 0.6; // allow a little extra to account for shrinkage
hbrb_magnet_bury_thickness = 0.6;  // want at least 2 layers to prevent magnet punch-through
hbrb_use_magnets = true;

hbrb_screwhole_diameter = 6;
hbrb_use_screwholes = false;

hbrb_pcb_size_x  = hbrb_relay_box ? 60 : hbrb_button_diameter*2.5;
hbrb_pcb_size_y  = hbrb_relay_box ? 56 : hbrb_button_diameter*2.5;
hbrb_pcb_thickness = 2;
hbrb_lid_plane_thickness = 2.0;
hbrb_base_plane_thickness = hbrb_use_magnets ? (hbrb_magnet_thickness + hbrb_magnet_bury_thickness) : 2.0;

hbrb_pin_diameter = 4;
hbrb_pin_offset  =  7;

hbrb_ridge_height = 4;
hbrb_round_radius = 1.5;
hbrb_wall_thickness = 2.0;
hbrb_wall_border = max(hbrb_wall_thickness, hbrb_round_radius);
hbrb_basehole_offset = hbrb_wall_border + (hbrb_use_magnets ? hbrb_magnet_diameter/2 : hbrb_screwhole_diameter*0.667);

hbrb_bottom_zsize_total = hbrb_bottom_zsize + hbrb_round_radius;
hbrb_lid_zsize_total = hbrb_lid_zsize + hbrb_round_radius;

hbrb_standoff_height = hbrb_bottom_zsize_total - hbrb_base_plane_thickness - hbrb_ridge_height;

hbrb_relay_width  = 15;
hbrb_relay_height = 8;
hbrb_relay_zbase = 0;

hbrb_usb_width  = 12;
hbrb_usb_height = 14;
hbrb_usb_zbase = hbrb_relay_height - 0.5;

hbrb_i2c_width  = 5;
hbrb_i2c_height = 3;
hbrb_i2c_zbase =  0;

hbrb_typeface = "Arial Black:style=regular";
hbrb_text_engrave_fraction = 0.75;

include <YAPP_Box-1.6/library/YAPPgenerator_v16.scad>

printBaseShell      = true;
printLidShell       = true;

wallThickness       = hbrb_wall_thickness;
basePlaneThickness  = hbrb_base_plane_thickness;
lidPlaneThickness   = hbrb_lid_plane_thickness;

// These two heights were obtained by trial and error. The cutouts on
// the sides are either exactly at the seam where the base and lid
// join, or the seam splits the cutouts and shares it between base and
// lid. That allows you to print the whole enclosure without supports.

baseWallHeight      = hbrb_bottom_zsize;
lidWallHeight       = hbrb_lid_zsize;

ridgeHeight         = hbrb_ridge_height;
ridgeSlack          = 1.5;
roundRadius         = hbrb_round_radius;

standoffHeight      = hbrb_bottom_zsize_total - hbrb_base_plane_thickness - hbrb_ridge_height - hbrb_pcb_thickness;
pinDiameter         = hbrb_pin_diameter - 0.4;  // avoid fitting too snugly
pinHoleSlack        = 0.3;
standoffDiameter    = pinDiameter + 1.5;

pcbLength           = hbrb_pcb_size_x;
pcbWidth            = hbrb_pcb_size_y;
pcbThickness        = hbrb_pcb_thickness;
                            
// We don't need padding because we're not concerned about the corners.
paddingFront        = 0;
paddingBack         = 0;
paddingRight        = 0;
paddingLeft         = 0;

//-- D E B U G -----------------//-> Default ---------
//showSideBySide      = false;     //-> true
onLidGap            = 0; //-> 3;
//shiftLid            = 1;
//hideLidWalls        = false;    //-> false
//colorLid            = "yellow";   
//hideBaseWalls       = false;    //-> false
//colorBase           = "white";
//showOrientation     = true;
//showPCB             = true;
//showPCBmarkers      = true;
//showShellZero       = true;
//showCenterMarkers   = true;
//inspectX            = 0;        //-> 0=none (>0 from front, <0 from back)
//inspectY            = 0;        //-> 0=none (>0 from left, <0 from right)
//-- D E B U G ---------------------------------------


pcbStands = [
   [            hbrb_pin_offset,            hbrb_pin_offset, yappBoth, yappPin]
  ,[pcbLength - hbrb_pin_offset, pcbWidth - hbrb_pin_offset, yappBoth, yappPin]
  ,[            hbrb_pin_offset, pcbWidth - hbrb_pin_offset, yappBoth, yappPin]
  ,[pcbLength - hbrb_pin_offset,            hbrb_pin_offset, yappBoth, yappPin]
];

//left and right: parallel to X-axis
//back and front: parallel to Y-axis
//length is X, width is Y

//-- left plane   -- origin is pcb[0,0,0]
// (0) = posx
// (1) = posz
// (2) = width
// (3) = height
// (4) = angle
// (5) = { yappRectangle | yappCircle }
// (6) = { yappCenter }
cutoutsLeft = hbrb_relay_box ? [
   [pcbLength/2 - hbrb_usb_width/2+0.5,   hbrb_usb_zbase,   hbrb_usb_width,   hbrb_usb_height,   0, yappRectangle]
  ,[pcbLength/2 - hbrb_relay_width/2,     hbrb_relay_zbase, hbrb_relay_width, hbrb_relay_height, 0, yappRectangle]
] : [];

//-- back plane  -- origin is pcb[0,0,0]
// (0) = posy
// (1) = posz
// (2) = width
// (3) = height
// (4) = angle
// (5) = { yappRectangle | yappCircle }
// (6) = { yappCenter }
cutoutsBack = hbrb_relay_box ? [
   [pcbWidth/2,     hbrb_i2c_zbase,               hbrb_i2c_width,       hbrb_i2c_height, 0, yappRectangle]
  ,[pcbWidth/2 + 7, hbrb_button_diameter/2 + 7.5, hbrb_button_diameter, 0,               0, yappCircle]
] : [];
cutoutsFront =  [
   [pcbLength*(1/2), hbrb_i2c_zbase, hbrb_i2c_width, hbrb_i2c_height, 0, yappRectangle]
];

//-- base plane    -- origin is pcb[0,0,0]
// (0) = posx
// (1) = posy
// (2) = width
// (3) = length
// (4) = angle
// (5) = { yappRectangle | yappCircle }
// (6) = { yappCenter }
// Five holes for maximum screw or magnet mounting flexibility.
cutoutsBase = hbrb_use_magnets ? 
     [
       [hbrb_basehole_offset, pcbWidth/2, hbrb_magnet_diameter, 0, 0, yappCircle]  // back
      ,[pcbLength - hbrb_basehole_offset, pcbWidth/2, hbrb_magnet_diameter, 0, 0, yappCircle] // front
      ,[pcbLength/2, hbrb_basehole_offset, hbrb_magnet_diameter, 0, 0, yappCircle] // left
      ,[pcbLength/2, pcbWidth-hbrb_basehole_offset, hbrb_magnet_diameter, 0, 0, yappCircle] // right
      ,[pcbLength/2, pcbWidth/2, hbrb_magnet_diameter, 0, 0, yappCircle] // center
    ]
  : hbrb_use_screwholes  ?
    [
       [hbrb_basehole_offset, pcbWidth/2, hbrb_screwhole_diameter, 0, 0, yappCircle]  // back
      ,[pcbLength - hbrb_basehole_offset, pcbWidth/2, hbrb_screwhole_diameter, 0, 0, yappCircle] // front
      ,[pcbLength/2, hbrb_basehole_offset, hbrb_screwhole_diameter, 0, 0, yappCircle] // left
      ,[pcbLength/2, pcbWidth-hbrb_basehole_offset, hbrb_screwhole_diameter, 0, 0, yappCircle] // right
      ,[pcbLength/2, pcbWidth/2, hbrb_screwhole_diameter, 0, 0, yappCircle] // center
] : [];

cutoutsLid = hbrb_relay_box ? [] : [
   [pcbLength/2, pcbWidth/2, hbrb_button_diameter, 0, 0, yappCircle] // center
];

// Little tabs for holding the two halves of the case together.
snapJoins   =   [
   [pcbLength/2 ,   4, yappRight]
  ,[pcbWidth*(1/4), 4, yappFront]
  ,[pcbWidth*(1/4), 4, yappBack]
];
               
//-- origin of labels is box [0,0,0]
// (0) = posx
// (1) = posy/z
// (2) = orientation
// (3) = depth
// (4) = plane {lid | base | left | right | front | back }
// (5) = font
// (6) = size
// (7) = "label text"
// trial and error positions since no way to calculate rendered sizes in OpenSCAD, and YAPP is always left-justified
labelsPlane = hbrb_relay_box ? [
   [ 8,  pcbLength/2 - 2,      0, lidPlaneThickness * hbrb_text_engrave_fraction, "lid",  hbrb_typeface, 6, "Hot Button" ]

  ,[14,  roundRadius+16,  0, lidPlaneThickness * hbrb_text_engrave_fraction, "left",  hbrb_typeface, 3, "NC" ]
  ,[44,  roundRadius+16,  0, lidPlaneThickness * hbrb_text_engrave_fraction, "left",  hbrb_typeface, 3, "NO" ]

  ,[14,  hbrb_bottom_zsize+hbrb_lid_zsize - 4,  0, lidPlaneThickness * hbrb_text_engrave_fraction, "front",  hbrb_typeface, 3, "D5 SCL yellow" ]
  ,[14,  hbrb_bottom_zsize+hbrb_lid_zsize - 8,  0, lidPlaneThickness * hbrb_text_engrave_fraction, "front",  hbrb_typeface, 3, "D2 SDA" ]
  ,[14,  hbrb_bottom_zsize+hbrb_lid_zsize -13,  0, lidPlaneThickness * hbrb_text_engrave_fraction, "front",  hbrb_typeface, 3, "D7 button" ]

  ,[12,  roundRadius+26,  0, lidPlaneThickness * hbrb_text_engrave_fraction, "right",  hbrb_typeface, 3, "https://hackaday.io/" ]
  ,[16,  roundRadius+20,  0, lidPlaneThickness * hbrb_text_engrave_fraction, "right",  hbrb_typeface, 3, "project/192719" ]

// if you are changing things and lose track of which side is which, you can uncomment these temporarily
//, [ 8,   5,   0, 1, "left",  hbrb_typeface, 7, "L" ]
//, [40,   5,   0, 1, "front", hbrb_typeface, 7, "F" ]
//, [5,    5,   0, 1, "back",  hbrb_typeface, 7, "B" ]

] : [];

//---- This is where the magic happens ----
YAPPgenerate();
if (hbrb_use_magnets) {
  color("blue")
    translate([hbrb_wall_border,hbrb_wall_border,0])
    cube(size = [shellLength-(2*hbrb_wall_border),shellWidth-(2*hbrb_wall_border),hbrb_magnet_bury_thickness], center = false);
 }
