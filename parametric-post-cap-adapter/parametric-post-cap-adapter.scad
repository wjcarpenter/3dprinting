/* [Explanation] */

// I wanted to put some solar LED lights on my deck posts. The posts are 3.5 inches on a side (nominal), and the lights I wanted to use want to be on a 4 inch (nominal) post. This like square ring adapts the size. Because the post measurements have a little variation in them, I made it parametric. You can give the measurements in either inches or millimeters. These are all actual dimensions, not nominal. You will probably have to experiment with a few test prints to get it right.
comment = 1; // 

/* [Configuration] */
// Measurements are inches (true) or millimeters (false)
use_inches = true;
// The post itself is this size on each side. Actual size, not nominal. Err toward being larger.
post_side_measure = 3.401;
// The light expects to be mounted on a post of this size on each side. Actual size, not nominal. Err toward being smaller.
light_side_measure = 3.521;
// Make the main body of the adapter this high/tall.
adapter_side_height = 0.791;
// A few layers on the top of the adapter are inset to keep it from falling down the post. This is a delta/difference; in other words, the subtracted out square is this much smaller. Each side is smaller by half this amount.
adapter_top_delta = 2.911;
// How high/thick is that top piece?
adapter_top_height = 0.061;
// Should to top piece have holes for nails/screws? If yes, there will be one on each side, midway in the adapter top delta.
use_top_holes = true;
// The diameter of the nail/screw holes in the top.
top_hole_diameter = 0.161;

/* [Hidden] */
// Because typical 3D printing is always done in millimeters, convert inch
// measurements to millimeters.

post_side_measure_mm   = (use_inches) ? (post_side_measure) * 25.4   : post_side_measure;
light_side_measure_mm  = (use_inches) ? (light_side_measure) * 25.4  : light_side_measure;
adapter_side_height_mm = (use_inches) ? (adapter_side_height) * 25.4 : adpater_side_height;
adapter_top_delta_mm   = (use_inches) ? (adapter_top_delta) * 25.4   : adpater_top_delta;
adapter_top_height_mm  = (use_inches) ? (adapter_top_height) * 25.4  : adpater_top_height;
top_hole_diameter_mm   = (use_inches) ? (top_hole_diameter) * 25.4   : top_hole_diameter;

module make_square_minus_square(outer, inner, height) {
  translate([0, 0, height/2]) {
    difference() {
      cube(size = [outer, outer, height], center = true);
      cube(size = [inner, inner, height], center = true);
    }
  }
}

module make_the_top_holes() {
  how_far_from_center = post_side_measure_mm/2 - adapter_top_delta_mm/4;
  how_far_from_centerY = 20;
  translate([how_far_from_center, 0, 0]) {
    color("black") cylinder($fn=25, h=adapter_top_height_mm, r1=top_hole_diameter_mm/2, r2=top_hole_diameter_mm/2, center=false);
  }
  translate([-how_far_from_center, 0, 0]) {
    color("black") cylinder($fn=25, h=adapter_top_height_mm, r1=top_hole_diameter_mm/2, r2=top_hole_diameter_mm/2, center=false);
  }
  translate([0, how_far_from_center, 0]) {
    color("black") cylinder($fn=25, h=adapter_top_height_mm, r1=top_hole_diameter_mm/2, r2=top_hole_diameter_mm/2, center=false);
  }
  translate([0, -how_far_from_center, 0]) {
    color("black") cylinder($fn=25, h=adapter_top_height_mm, r1=top_hole_diameter_mm/2, r2=top_hole_diameter_mm/2, center=false);
  }
}

module make_the_adapter() {
  // we draw it upside down because that's how we'll print it.
  color("orange") make_square_minus_square(light_side_measure_mm, post_side_measure_mm - adapter_top_delta_mm, adapter_top_height_mm);
  translate([0, 0, adapter_top_height_mm]) {
    color("blue") make_square_minus_square(light_side_measure_mm, post_side_measure_mm, adapter_side_height_mm);
  }
}

if (use_top_holes) {
  difference() {
    make_the_adapter();
    make_the_top_holes();
  }
} else {
  make_the_adapter();
}
